package com.example.formdevelopment.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.formdevelopment.fragment.LoginFragment;
import com.example.formdevelopment.fragment.SignupFragment;


public class TabsAdapter extends FragmentStatePagerAdapter {

    int mNoOfTabs;

    public TabsAdapter(FragmentManager fm, int mNoOfTabs) {
        super(fm);
        this.mNoOfTabs = mNoOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                LoginFragment login=new LoginFragment();
                return login;
            case 1:
                SignupFragment signup=new SignupFragment();
                return signup;
                default:
                    return null;
        }
    }

    @Override
    public int getCount() {
        return mNoOfTabs;
    }
}
