package com.example.formdevelopment.fragment;


import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.example.formdevelopment.activity.MainActivity;
import com.example.formdevelopment.R;
import com.example.formdevelopment.helper.DbHelper;


/**
 * A simple {@link Fragment} subclass.
 */
public class SignupFragment extends Fragment {


    public SignupFragment() {
        // Required empty public constructor
    }
    DatePickerDialog picker;
    EditText eText;
    private EditText sMobileNo, sEmail, sFullName,sPassword,sConfirmPassword;
    private Button signupBtn;
    private ProgressBar progressBar;
    private TextView sPrivacyPolicy, sTermsAndCondition;
    private String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+.[a-z]+";
    DbHelper mydb;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_signup, container, false);
        mydb=new DbHelper(getContext());
        sMobileNo = view.findViewById(R.id.signup_mobile_no);
        sEmail = view.findViewById(R.id.signup_email_id);
        sFullName = view.findViewById(R.id.signup_first_name);
        sPassword = view.findViewById(R.id.signup_password);
        sConfirmPassword=view.findViewById(R.id.signup_confirm_password);
        progressBar = view.findViewById(R.id.signup_progressbar);
        signupBtn = view.findViewById(R.id.signup_btn);
        signupBtn.setEnabled(true);

        setEmail();
        return view;
    }

    private void setEmail() {
        signupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //checkEmailAndPassword();
                checkEmailAndPassword();
            }
        });
    }



    private void checkEmailAndPassword() {


        if (sEmail.getText().toString().matches(emailPattern)) {

            progressBar.setVisibility(View.VISIBLE);


            final String MobileNo = sMobileNo.getText().toString();
            final String Signup_Email = sEmail.getText().toString();
            final String FullName = sFullName.getText().toString();
            final String Signup_password = sPassword.getText().toString();
            final String confirm_password=sConfirmPassword.getText().toString();

            if (Signup_Email.equals("") || Signup_password.equals("") || confirm_password.equals("")){
                Toast.makeText(getContext(), "Insert Data Properly", Toast.LENGTH_SHORT).show();
            } else {
                if (Signup_password.equals(confirm_password)){
                    Boolean chkEmail=mydb.checkEmail(Signup_Email);

                    if (chkEmail==true){
                        Boolean insert =mydb.insertData(Signup_Email,Signup_password);
                        if (insert==true){
                            Toast.makeText(getContext(), "Registration Successfully", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getContext(), MainActivity.class);
                            startActivity(intent);
                        }
                    } else {
                        Toast.makeText(getContext(), "Email Already Exists", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getContext(), "Password Doesn't Match", Toast.LENGTH_SHORT).show();
                }
            }

        }


    }
}
