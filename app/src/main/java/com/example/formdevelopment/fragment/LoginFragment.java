package com.example.formdevelopment.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.example.formdevelopment.activity.MainActivity;
import com.example.formdevelopment.R;
import com.example.formdevelopment.helper.DbHelper;


/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment {

    private EditText loginEmail, loginPassword;
    private Button loginBtn;
    private TextView loginwithOtp, forgotPassword;
    DbHelper myDb;


    public LoginFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        myDb=new DbHelper(getContext());
        loginEmail = view.findViewById(R.id.login_email_id);
        loginPassword = view.findViewById(R.id.login_password);
        loginBtn = view.findViewById(R.id.login_btn);

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userLogin();

            }
        });
        return view;
    }

    private void userLogin(){
        final String email=loginEmail.getText().toString();
        final String password=loginPassword.getText().toString();
        if (email.equals("") || password.equals("")){
            Toast.makeText(getContext(), "Insert Data", Toast.LENGTH_SHORT).show();
        } else {
            Boolean checkPassword=myDb.checkPassword(email,password);

            if (checkPassword==true){

                Toast.makeText(getContext(), "Insert Correct email or Password", Toast.LENGTH_SHORT).show();
            } else {

                Toast.makeText(getContext(), "Registration Successfully", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getContext(), MainActivity.class);
                startActivity(intent);
            }
        }
    }
}
