package com.example.formdevelopment.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DbHelper extends SQLiteOpenHelper {





    public DbHelper(Context context){
        super(context,"login.db",null,1);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table user(email text primary key,password text)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS user");
    }

    public boolean insertData(String email,String userPassword){
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues contentValues=new ContentValues();
        contentValues.put("email",email);
        contentValues.put("password",userPassword);

        long result=db.insert("user",null,contentValues);
        db.close();

        if (result==-1){
            return false;
        } else {
            return true;
        }
    }

    public Cursor getAllData(){
        SQLiteDatabase db=this.getWritableDatabase();
        Cursor res=db.rawQuery("Select * from " +"user",null);
        return res;
    }

    public boolean checkEmail(String email){
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor res=db.rawQuery("Select * from user where email=?",new String[]{email});
        if (res.getCount()>0) return false;
        else return true;
    }
    public boolean checkPassword(String email,String password){
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor res=db.rawQuery("Select * from user where email=? AND password = ?",new String[]{email,password});
        if (res.getCount()>0) return false;
        else return true;
    }
}
