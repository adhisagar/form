package com.example.formdevelopment.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.example.formdevelopment.R;
import com.example.formdevelopment.helper.RotateBitmap;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {
    public static final int SELECT_PICTURE = 100;
    private static final int STORAGE_PERMISSION_CODE = 111;

    private ImageView user_image;
    private Button imagePickerBtn;
    Bitmap bitmap;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        user_image=findViewById(R.id.image_pick);
        imagePickerBtn=findViewById(R.id.img_picker_btn);

        imagePickerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openImageChooser();
                }
        });

    }

    void openImageChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_PICTURE) {
                // Get the url from data
                Uri selectedImageUri;
                selectedImageUri = data.getData();

                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImageUri);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) { ;
                        user_image.setImageBitmap(new RotateBitmap().HandleSamplingAndRotationBitmap(MainActivity.this,selectedImageUri));
                    }

                    user_image.setImageBitmap(bitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                }

//                if (null != selectedImageUri) {
//                    // Get the path from the Uri
//                    String path = getPathFromURI(selectedImageUri);
//                    Log.i("tag", "Image Path : " + path);
//                    // Set the image in ImageView
//                    driverImage.setImageURI(selectedImageUri);
//
//                   // imagePickerBtn.setText(path);
//                }
            }

        }
    }
}
